DOC   = inf

all: $(DOC).pdf

clean:
	rm -f $(DOC).{aux,bbl,blg,dvi,lof,log,lot,out,toc}

distclean: clean
	rm -f $(DOC).pdf

.PHONY: all clean distclean

%.pdf: %.tex
	pdflatex -halt-on-error $<
	pdflatex -halt-on-error $<
